import { computed } from "@angular/core";
import { getState, patchState, signalStore, withComputed, withHooks, withMethods, withState } from "@ngrx/signals";
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tap } from "rxjs";

type StoreState = {
    firstName: string,
    lastName: string
}

const initialState: StoreState = {
    firstName: "",
    lastName: ""
}

export const NameStore = signalStore(

    { providedIn: "root" },

    withState<StoreState>(initialState),

    withMethods((store) => ({
        setState(newState: Partial<StoreState>): void {
            patchState(store, newState)
        },

        printStore(): void {
            console.log(getState(store));
        },

        resetStore(): void {
            patchState(store, initialState)
        },

        rxMethodTest: rxMethod((o$) => o$.pipe(
            tap((value) => console.log(value))
        ))
    })),

    withHooks(() => ({
        onInit: () => {
            console.log("Store initiated");
        },

        onDestroy: () => {
            console.log("Store destroyed");
        }
    })),

    withComputed(({firstName, lastName}) => ({
        fullName: computed(() => `${firstName()} ${lastName()}`)
    }))

)
