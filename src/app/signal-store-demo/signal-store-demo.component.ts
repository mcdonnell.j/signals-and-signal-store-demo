import { Component, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { NameStore } from './name.store';
import { MatInput } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field'
import {FormsModule} from '@angular/forms';;


@Component({
  selector: 'signal-store-demo',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatInput, MatFormFieldModule, FormsModule],
  templateUrl: './signal-store-demo.component.html',
  styleUrls: ['./signal-store-demo.component.css']
})
export class SignalStoreDemoComponent {
  nameStore = inject(NameStore)
}
