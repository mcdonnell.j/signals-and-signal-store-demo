import { Component, Signal, WritableSignal, computed, effect, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button'
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';

@Component({
  selector: 'signal-demo',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatFormFieldModule, MatSelectModule, MatInputModule],
  templateUrl: './signal-demo.component.html',
  styleUrls: ['./signal-demo.component.css'],
})
export class SignalDemoComponent {
  initialSignalValue: number = 0;

  myValue: WritableSignal<number> = signal(this.initialSignalValue);

  sum: Signal<number> = computed(() => this.myValue() + 10)

  constructor() {
    effect(() => {
      console.log(`myValue changed to: ${this.myValue()}`);
    })
  }

  increment() {
    this.myValue.update(value => value + 1);
  }

  decrement() {
    this.myValue.update(value => value - 1);
  }

  reset() {
    this.myValue.set(this.initialSignalValue);
  }
}
