import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignalDemoComponent } from './signal-demo/signal-demo.component';
import { SignalStoreDemoComponent } from './signal-store-demo/signal-store-demo.component';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-root',
  standalone: true,
  providers: [SignalDemoComponent],
  imports: [CommonModule, SignalDemoComponent, SignalStoreDemoComponent, MatDividerModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  SignalDemo = inject(SignalDemoComponent)
  title = 'signal-demo';
}
